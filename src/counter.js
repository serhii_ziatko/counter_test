export default class Counter {

    constructor(elem, options  ={}) {
        if(elem instanceof HTMLElement !== true){
            throw new Error('Valid element should be passed');
        }
        this.init(options)
        this.render(elem)
        this.initEvents()
    }

    init(options) {
        const {
            name = '',
            inc = 1,
            initValue = 0,
            maxVal = 10,
            minVal = 0
        } = options

        this.name = name
        this.inc = inc
        this.value = initValue
        this.maxVal = maxVal
        this.minVal = initValue
    }

    initEvents() {
        this.cmpRoot.getElementsByClassName('js-decrement')[0].addEventListener('click', function(e){
            this.onDecrement(e)
        }.bind(this))

        this.cmpRoot.getElementsByClassName('js-increment')[0].addEventListener('click', function(e){
            this.onIncrement(e)
        }.bind(this))
        this.cmpRoot.getElementsByClassName('js-set-max')[0].addEventListener('click', function(e){
            this.onSetMax(e)
        }.bind(this))
        this.cmpRoot.getElementsByClassName('js-set-min')[0].addEventListener('click', function(e){
            this.onSetMin(e)
        }.bind(this))
    }

    onDecrement(e) {
        e.preventDefault()

        const newVal = this.value - this.inc * 1
        if(newVal < 0){
            return
        }
        this.value = newVal
        this.cmpRoot.getElementsByClassName('js-val')[0].innerHTML = this.value
    }

    onIncrement(e) {
        e.preventDefault()

        this.value = this.value + this.inc * 1
        this.cmpRoot.getElementsByClassName('js-val')[0].innerHTML = this.value
    }

    render(elem) {
        let template = `<div class="counter">
            <div class="counter__title">${this.name}</div>
                <div class="js-val">${this.value}</div>
                <div>
                    <button class="js-decrement">-</button><button class="js-increment">+</button>
                </div>
                <div>
                    <button class="js-set-max">Set max</button><button class="js-set-min">Set min</button>
                </div>
        </div>`
        elem.innerHTML = template

        this.cmpRoot = document.getElementsByClassName('counter')[0]
    }

    onSetMax(e){
      e.preventDefault();

      this.value = this.maxVal;
      this.cmpRoot.getElementsByClassName('js-val')[0].innerHTML = this.value

    }
    onSetMin(e){
      e.preventDefault();

      this.value = this.minVal;
      this.cmpRoot.getElementsByClassName('js-val')[0].innerHTML = this.value

    }


}
