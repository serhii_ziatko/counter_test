import Counter from './counter';

let elem = null;

const createElem = () => {
  elem = document.createElement('div');
  elem.id = 'root';
  document.body.appendChild( elem );
}

const destroyElem = () => {
  document.body.removeChild( elem );
  elem = null;
}
const defaultConfig = {
  name: 'Cool Counter',
  inc: 1,
  initValue: 1,
  maxVal: 10
}

let counterCmp;

const createComponent = () => {
  counterCmp = new Counter(elem, defaultConfig);
};

const destroyComponent = () => {
  counterCmp = null
};








describe('Counter', () =>{
  // beforeEach beforeAll afterEach afterAll
  beforeEach( createElem );
  afterEach( destroyElem );

  it('Should be created', () => {
    let counter = new Counter(
     elem,
      {
        name: 'Cool Counter',
        inc: 1,
        initValue: 1
      }
    )
    expect(counter).toBeDefined();
    expect(counter.name).toMatch(defaultConfig.name);
    expect(counter.inc).toEqual(defaultConfig.inc);
    expect(counter.value).toEqual(defaultConfig.initValue);
    expect(counter.maxVal).toEqual(defaultConfig.maxVal);
  });

  it( 'should be created with default options', () => {
    let counter = new Counter(elem);
    expect(counter).toBeDefined();
    expect(counter.name).toMatch('');
    expect(counter.inc).toEqual(1);
    expect(counter.value).toEqual(0);
    expect(counter.maxVal).toEqual(10);
  });

  it('should throw without HTMLElement', () => {
    expect(() => {
      new Counter()
    }).toThrow();
  });

});


describe('test initial methods', () => {
   beforeEach( () =>{
     createElem();
     createComponent();
   });
   afterEach( () => {
     destroyElem();
     destroyComponent();
   });



   describe('test Counter render', () =>{

      it( 'should create root element', () => {
        expect(counterCmp.cmpRoot instanceof HTMLElement).toBe(true);
      });

      it( 'should have correct title', () => {
        let title = counterCmp.cmpRoot.getElementsByClassName('counter__title')[0].innerText;
        expect(title).toMatch(defaultConfig.name);
      });

      it( 'should have correct title', () => {
        let value = counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText;
        expect(value).toEqual(defaultConfig.initValue + '');
      });

    });


    describe('test initEvents', () => {
      beforeEach( () =>{
        createComponent();
      });

      it('should call initEvents', () => {
        let eventsSpy = spyOn(Counter.prototype, 'initEvents');
        createComponent();

        expect(eventsSpy).toHaveBeenCalled();
      });

      it('should increment', () => {
        let value_before_click = counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText;

        let incrementSpy = spyOn(Counter.prototype, 'onIncrement').and.callThrough();
        createComponent();

        let e = new MouseEvent('click');
        counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e);

        let value_after_click = counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText;

        expect(incrementSpy).toHaveBeenCalled();
        expect(+value_after_click).toEqual(+value_before_click + counterCmp.inc);
      })

    });

    describe('Set max/min ', () => {
      it('should set max value on click', () => {
        destroyComponent();
        let setMaxSpy = spyOn( Counter.prototype, 'onSetMax').and.callThrough();
        createComponent();

        let e = new MouseEvent('click');
        counterCmp.cmpRoot.getElementsByClassName('js-set-max')[0].dispatchEvent(e);

        expect(setMaxSpy).toHaveBeenCalled();
        expect(counterCmp.value).toEqual(counterCmp.maxVal);
        expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText)
          .toEqual(counterCmp.maxVal);
      })


      it('should set min value on click', () => {
        destroyComponent();
        let setMinSpy = spyOn( Counter.prototype, 'onSetMin').and.callThrough();
        createComponent();

        let e = new MouseEvent('click');
        counterCmp.cmpRoot.getElementsByClassName('js-set-min')[0].dispatchEvent(e);

        expect(setMinSpy).toHaveBeenCalled();
        expect(counterCmp.value).toEqual(counterCmp.minVal);
        expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText)
          .toEqual(counterCmp.minVal);
      })
    });

});





