import Counter from './counter'


new Counter(document.getElementById('root'), {
    name: 'Cool Counter',
    inc: 1,
    initValue: 1
})
